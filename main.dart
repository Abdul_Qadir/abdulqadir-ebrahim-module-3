import 'package:flutter/material.dart';
import 'login.dart';
import 'sign_up.dart';
import 'dash_board.dart';
import 'profile.dart';
import 'feature_1.dart';
import 'feature_2.dart';

void main() {
  runApp(
    MaterialApp(
      title: 'Module 3',
      // Start the app with the "/" named route. In this case, the app starts
      // on the FirstScreen widget.
      initialRoute: '/',
      routes: {
        // When navigating to the "/" route, build the FirstScreen widget.
        '/': (context) => const HomeScreen(),
        // When navigating to the "/second" route, build the SecondScreen widget.
        '/Login': (context) => const Login(),
        '/RegistrationScreen': (context) => const RegistrationScreen(),
        '/DashBoard': (context) => const DashBoard(),
        '/Feature_1': (context) => const FeatureOne(),
        '/Feature_2': (context) => const FeatureTwo(),
        '/Profile': (context) => const Profile(),
      },
    ),
  );
}

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text('Home Screen')),
        body: Center(
          child: Column(children: <Widget>[
            Container(
                margin: const EdgeInsets.all(5),
                child: ElevatedButton(
                  onPressed: () {
                    Navigator.pushNamed(context, '/Login');
                  },
                  child: const Text('Login'),
                )),
            Container(
                margin: const EdgeInsets.all(5),
                child: ElevatedButton(
                  onPressed: () {
                    Navigator.pushNamed(context, '/RegistrationScreen');
                  },
                  child: const Text('Register'),
                )),
          ]),
        ));
  }
}
