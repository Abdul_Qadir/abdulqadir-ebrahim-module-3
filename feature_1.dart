import 'package:flutter/material.dart';

void main() => runApp(const FeatureOne());

class FeatureOne extends StatelessWidget {
  const FeatureOne({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Feature Screen 1'),
        ),
        floatingActionButton: Container(
          height: 100,
          width: 200,
          child: FittedBox(
              child: FloatingActionButton(
            shape:
                const BeveledRectangleBorder(borderRadius: BorderRadius.zero),
            onPressed: () {
              Navigator.pushNamed(context, '/Feature_2');
            },
            child: const Text('Feature Screen 2'),
          )),
        ));
  }
}
