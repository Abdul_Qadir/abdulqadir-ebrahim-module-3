import 'package:flutter/material.dart';

void main() => runApp(const Profile());

class Profile extends StatelessWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('My Profile'),
      ),
      body: Center(
          child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        const MyProfileForm(),
        ElevatedButton(
          onPressed: () {
            Navigator.pushNamed(context, '/');
          },
          child: const Text('Save', style: TextStyle(fontSize: 20)),
        ),
      ])),
    );
  }
}

class MyProfileForm extends StatelessWidget {
  const MyProfileForm({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
        Widget>[
      const Padding(
        padding: EdgeInsets.symmetric(horizontal: 500, vertical: 16),
        child: TextField(
          decoration: InputDecoration(
            border: OutlineInputBorder(),
            hintText: 'First Name',
          ),
        ),
      ),
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 500, vertical: 16),
        child: TextFormField(
          decoration: const InputDecoration(
            border: OutlineInputBorder(),
            labelText: 'Last Name',
          ),
        ),
      ),
      Padding(
          padding: const EdgeInsets.symmetric(horizontal: 500, vertical: 16),
          child: TextFormField(
              decoration: const InputDecoration(
            border: OutlineInputBorder(),
            labelText: 'Email',
          ))),
      Padding(
          padding: const EdgeInsets.symmetric(horizontal: 500, vertical: 16),
          child: TextFormField(
              decoration: const InputDecoration(
            border: OutlineInputBorder(),
            labelText: 'User Name',
          ))),
      Padding(
          padding: const EdgeInsets.symmetric(horizontal: 500, vertical: 16),
          child: TextFormField(
              decoration: const InputDecoration(
            border: OutlineInputBorder(),
            labelText: 'Password',
          )))
    ]);
  }
}
