import 'package:flutter/material.dart';

void main() => runApp(const RegistrationScreen());

class RegistrationScreen extends StatelessWidget {
  const RegistrationScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Registration Screen'),
      ),
      body: Center(
          child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        const MyRegistrationForm(),
        ElevatedButton(
          onPressed: () {
            Navigator.pushNamed(context, '/DashBoard');
          },
          child: const Text('Register', style: TextStyle(fontSize: 20)),
        ),
      ])),
    );
  }
}

class MyRegistrationForm extends StatelessWidget {
  const MyRegistrationForm({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        const Padding(
          padding: EdgeInsets.symmetric(horizontal: 500, vertical: 16),
          child: TextField(
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              hintText: 'First Name',
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 500, vertical: 16),
          child: TextFormField(
            decoration: const InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Last Name',
            ),
          ),
        ),
        Padding(
            padding: const EdgeInsets.symmetric(horizontal: 500, vertical: 16),
            child: TextFormField(
                decoration: const InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Email',
            ))),
        Padding(
            padding: const EdgeInsets.symmetric(horizontal: 500, vertical: 16),
            child: TextFormField(
                decoration: const InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Password',
            ))),
      ],
    );
  }
}
