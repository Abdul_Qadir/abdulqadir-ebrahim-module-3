import 'package:flutter/material.dart';

void main() => runApp(const FeatureTwo());

class FeatureTwo extends StatelessWidget {
  const FeatureTwo({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Feature Screen 2'),
        ),
        body: Center(
          child: Column(children: <Widget>[
            Container(
                margin: const EdgeInsets.all(5),
                child: ElevatedButton(
                  onPressed: () {
                    Navigator.pushNamed(context, '/Profile');
                  },
                  child: const Text('Profile'),
                )),
          ]),
        ));
  }
}
