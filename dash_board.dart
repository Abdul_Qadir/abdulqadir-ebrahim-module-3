import 'package:flutter/material.dart';

void main() => runApp(const DashBoard());

class DashBoard extends StatelessWidget {
  const DashBoard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Dashboard'),
        ),
        body: const Center(),
        floatingActionButton: Container(
          height: 100,
          width: 200,
          child: FittedBox(
              child: FloatingActionButton(
            shape:
                const BeveledRectangleBorder(borderRadius: BorderRadius.zero),
            onPressed: () {
              Navigator.pushNamed(context, '/Feature_1');
            },
            child: const Text('Feature Screen 1'),
          )),
        ));
  }
}
