import 'package:flutter/material.dart';

void main() => runApp(const Login());

class Login extends StatelessWidget {
  const Login({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Login'),
      ),
      body: Center(
          child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        const MyLoginForm(),
        ElevatedButton(
          onPressed: () {
            Navigator.pushNamed(context, '/DashBoard');
          },
          child: const Text('Submit', style: TextStyle(fontSize: 20)),
        ),
      ])),
    );
  }
}

class MyLoginForm extends StatelessWidget {
  const MyLoginForm({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        const Padding(
          padding: EdgeInsets.symmetric(horizontal: 500, vertical: 10),
          child: TextField(
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              hintText: 'Email',
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 500, vertical: 10),
          child: TextFormField(
            decoration: const InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Password',
            ),
          ),
        ),
      ],
    );
  }
}

class ButtonBody extends StatelessWidget {
  const ButtonBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Sign In'),
        ),
        body: const Center(child: ButtonBody()));
  }
}

@override
Widget build(BuildContext context) {
  return Align(
    alignment: Alignment.bottomCenter,
    child: ElevatedButton(
      onPressed: () {},
      child: const Text('Submit', style: TextStyle(fontSize: 20)),
    ),
  );
}
